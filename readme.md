# Twitter research

Read this in other languages: [Русский](readme.ru.md)

System of public opinion monitoring in twitter by specified tags.
This application in real-time listens twitter stream by specified keywords or tags,
automatically estimates sentiment rate for each tweet and store result to the cassandra db

## Technologies stack
java 8, spring framework, multithreading, apache cassandra.  
Delta TF-IDF algorithm for building feature vector of text.  
My own implementation of naive Bayes classifiers for sentiment estimation.

## Run

1. Run apache cassandra
2. Run script ```src/main/resources/database_config/cassandra_init.sql```  using cqlsh: ```cqlsh -f cassandra_init.sql```
3. Build this project: ```mvn clean install```
4. Run it ```java -jar twitter_research.jar```