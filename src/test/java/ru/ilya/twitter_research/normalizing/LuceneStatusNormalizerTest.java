package ru.ilya.twitter_research.normalizing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:/spring-config/appContextTest.xml")
public class LuceneStatusNormalizerTest {

    private static String TWEET = "Коллеги сидят||| рубятся $#@%^&^$#~`а из за ///.^%()долбанной, винды не могу";
    private static String GARBAGE_STRING = "|||  $#@%^&^$#~` ///.^%() ";

    @Autowired
    private LuceneStatusNormalizer normalizer;

    @Test
    public void test_normalize() throws Exception {
        Assert.assertEquals("коллег сид руб долба винд мог", normalizer.normalize(TWEET));
    }

    @Test
    public void normalize_should_return_empty_string_on_null_value() throws Exception {
        Assert.assertEquals("", normalizer.normalize(null));
    }

    @Test
    public void normalize_should_return_empty_string_on_emptyString() throws Exception {
        Assert.assertEquals("", normalizer.normalize(""));
    }

    @Test
    public void normalize_should_return_empty_string_on_string_with_only_garbage_chars() throws Exception {
        Assert.assertEquals("", normalizer.normalize(GARBAGE_STRING));
    }



}