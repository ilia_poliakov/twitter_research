CREATE KEYSPACE twitter_research WITH REPLICATION = {
   'class' : 'SimpleStrategy',
   'replication_factor' : 2
  };

USE twitter_research;

CREATE TABLE tweet (
    topic text,
    created_at date,
    id bigint,
    hash_tags set<text>,
    normalized_text text,
    original_text text,
    sentiment_rate double,
    PRIMARY KEY (topic, created_at)
) WITH comment = 'twits for statistic';