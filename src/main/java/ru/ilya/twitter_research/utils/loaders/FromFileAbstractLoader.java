package ru.ilya.twitter_research.utils.loaders;

import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public abstract class FromFileAbstractLoader<T> {

    private Resource file;

    public FromFileAbstractLoader(Resource file) {
        this.file = file;
    }

    public Map<String, T> load() {
        Map<String, T> result = new HashMap<>(65000);
        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line;

            while ((line = in.readLine()) != null) {
                String[] spitedLine = line.split(",");
                result.put(spitedLine[0], extractOneLine(spitedLine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    protected abstract T extractOneLine(String[] line);
}
