package ru.ilya.twitter_research.utils.loaders;

import java.util.Map;

public interface MapLoader<V> {

    public Map<String, V> loadMap();
}
