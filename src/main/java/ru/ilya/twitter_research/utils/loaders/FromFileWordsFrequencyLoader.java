package ru.ilya.twitter_research.utils.loaders;

import org.springframework.core.io.Resource;
import ru.ilya.twitter_research.domain.Word;

import java.util.Map;

import static java.lang.Integer.parseInt;

public class FromFileWordsFrequencyLoader extends FromFileAbstractLoader<Word> implements MapLoader<Word> {

    public FromFileWordsFrequencyLoader(Resource file) {
        super(file);
    }

    @Override
    public Map<String, Word> loadMap() {
        return super.load();
    }

    @Override
    protected Word extractOneLine(String[] line) {
        return new Word(line[0], parseInt(line[1]), parseInt(line[2]));
    }

}
