package ru.ilya.twitter_research.domain;

public class Word {

    private String word;
    private int occurrenceInPositive;
    private int occurrenceInNegative;

    public Word(String word) {
        this.word = word;
    }

    public Word(String word, int occurrenceInPositive, int occurrenceInNegative) {
        this.word = word;
        this.occurrenceInPositive = occurrenceInPositive;
        this.occurrenceInNegative = occurrenceInNegative;
    }

    public String getWord() {
        return word;
    }

    public int getOccurrenceInPositive() {
        return occurrenceInPositive;
    }

    public int getOccurrenceInNegative() {
        return occurrenceInNegative;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word1 = (Word) o;

        return word.equals(word1.word);
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return "Word{" +
                "word='" + word + '\'' +
                ", occurrenceInPositive=" + occurrenceInPositive +
                ", occurrenceInNegative=" + occurrenceInNegative +
                '}';
    }
}
