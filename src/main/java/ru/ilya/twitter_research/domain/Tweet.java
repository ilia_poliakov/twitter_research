package ru.ilya.twitter_research.domain;

import java.util.Arrays;
import java.util.Date;

public class Tweet extends BaseEntity {

    private String topic;
    private String originalText;
    private String normalizedText;
    private double sentimentRate;
    private String[] hashTags;
    private Date createdAt;

    public Tweet(long id, String topic, String originalText, String[] hashTags, Date createdAt) {
        super(id);
        this.originalText = originalText;
        this.hashTags = hashTags;
        this.createdAt = createdAt;
        this.topic = topic;
    }

    public Tweet(long id, String topic, String originalText, String normalizedText, double sentimentRate, String[] hashTags, Date createdAt) {
        this(id, topic, originalText, hashTags, createdAt);
        this.normalizedText = normalizedText;
        this.sentimentRate = sentimentRate;
    }

    public String[] getHashTags() {
        return hashTags;
    }

    public String getOriginalText() {
        return originalText;
    }

    public double getSentimentRate() {
        return sentimentRate;
    }

    public void setSentimentRate(double sentimentRate) {
        this.sentimentRate = sentimentRate;
    }

    public String getNormalizedText() {
        return normalizedText;
    }

    public void setNormalizedText(String normalizedText) {
        this.normalizedText = normalizedText;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", originalText='" + originalText + '\'' +
                ", normalizedText='" + normalizedText + '\'' +
                ", sentimentRate=" + sentimentRate +
                ", hashTags=" + Arrays.toString(hashTags) +
                ", createdAt=" + createdAt +
                '}';
    }
}
