package ru.ilya.twitter_research.stages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.ilya.twitter_research.domain.Tweet;
import ru.ilya.twitter_research.normalizing.Normalizer;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;

@Component("preprocessingStage")
public class PreprocessingStage implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(PreprocessingStage.class);

    @Resource(name = "receivedStatuses")
    private BlockingQueue<Tweet> receivedStatuses;

    @Resource(name = "normalizedStatuses")
    private BlockingQueue<Tweet> normalizedStatuses;

    @Resource(name = "luceneStatusNormalizer")
    private Normalizer<String, String> normalizer;

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Tweet tweet = receivedStatuses.take();
                String statusText = tweet.getOriginalText();
                String normalizedStatusText = normalizer.normalize(statusText);
                tweet.setNormalizedText(normalizedStatusText);
                normalizedStatuses.put(tweet);

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.info("Preprocessor has been stopped");
            }
        }
    }
}
