package ru.ilya.twitter_research.stages;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ilya.twitter_research.domain.Tweet;
import twitter4j.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class TwitterStatusListenerStage extends StatusAdapter {

    private static final Logger logger = LoggerFactory.getLogger(TwitterStatusListenerStage.class);

    private TwitterStream twitterStream;
    private BlockingQueue<Tweet> receivedStatuses;
    private String topic;

    public TwitterStatusListenerStage(TwitterStream twitterStream, List<String> keyWords, BlockingQueue<Tweet> receivedStatuses, String topic) {
        this.receivedStatuses = receivedStatuses;
        this.topic = topic;
        twitterStream.addListener(this);
        twitterStream.filter(new FilterQuery()
                .track(keyWords.toArray(new String[0]))
                .language(new String[]{"ru"}));
        this.twitterStream = twitterStream;
    }

    @Override
    public void onStatus(Status status) {
        try {
            if (!status.isRetweet()) {
                receivedStatuses.put(statusToTweet(status));
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.info("Twitter listener has been stopped");
        }
    }

    @Override
    public void onException(Exception ex) {
        logger.error("Error while tweet receiving", ex);
        twitterStream.shutdown();
        Thread.currentThread().interrupt();
    }

    private Tweet statusToTweet(Status status) {
        String[] hashtags = Arrays.stream(status.getHashtagEntities())
                .map(HashtagEntity::getText)
                .toArray(String[]::new);

        return new Tweet(status.getId(), topic, status.getText(), hashtags, status.getCreatedAt());
    }
}
