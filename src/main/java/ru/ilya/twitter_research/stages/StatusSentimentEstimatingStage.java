package ru.ilya.twitter_research.stages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.ilya.twitter_research.domain.Tweet;
import ru.ilya.twitter_research.sentiment_estimators.SentimentEstimator;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;

@Component("statusSentimentEstimatingStage")
public class StatusSentimentEstimatingStage implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(StatusSentimentEstimatingStage.class);

    @Resource(name = "normalizedStatuses")
    private BlockingQueue<Tweet> normalizedStatuses;

    @Resource(name = "estimatedStatuses")
    private BlockingQueue<Tweet> estimatedStatuses;

    @Resource(name = "bayesSentimentEstimator")
    private SentimentEstimator sentimentEstimator;

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Tweet tweet = normalizedStatuses.take();
                double sentimentRate = sentimentEstimator.estimate(tweet.getNormalizedText());
                tweet.setSentimentRate(sentimentRate);

                estimatedStatuses.put(tweet);

            } catch(InterruptedException e){
                Thread.currentThread().interrupt();
                logger.info("Twitter sentiment estimator has been stopped");
            }
        }
    }
}
