package ru.ilya.twitter_research.stages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ilya.twitter_research.domain.Tweet;
import ru.ilya.twitter_research.repository.Repository;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;

@Component("savingToDbStage")
public class SavingToDbStage implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(SavingToDbStage.class);

    @Resource(name = "estimatedStatuses")
    private BlockingQueue<Tweet> estimatedStatuses;

    @Autowired
    private Repository<Tweet> tweetRepository;

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Tweet tweet = estimatedStatuses.take();
                tweetRepository.insert(tweet);

                logger.debug("tweet " + tweet + "has been saved to db");
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                logger.info("Twitter saver has been stopped");
            }
        }
    }
}
