package ru.ilya.twitter_research.database_config;


import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:database_config/cassandra.properties")
public class CassandraConfig {

    @Value("${address}")
    private String address;

    @Value("${keyspace}")
    private String keyspace;

    @Value("${coreLocalConnections}")
    private int coreLocalConnections;

    @Value("${maxLocalConnections}")
    private int maxLocalConnections;

    @Value("${coreRemoteConnections}")
    private int coreRemoteConnections;

    @Value("${maxRemoteConnections}")
    private int maxRemoteConnections;

    @Bean
    public PoolingOptions poolingOptions() {
        return new PoolingOptions()
                .setCoreConnectionsPerHost(HostDistance.LOCAL,  coreLocalConnections)
                .setMaxConnectionsPerHost(HostDistance.LOCAL, maxLocalConnections)
                .setCoreConnectionsPerHost(HostDistance.REMOTE, coreRemoteConnections)
                .setMaxConnectionsPerHost(HostDistance.REMOTE, maxRemoteConnections);
    }

    @Bean
    public Cluster cluster() {
        return Cluster.builder()
                .withPoolingOptions(poolingOptions())
                .addContactPoint(address)
                .build();
    }

    @Bean
    public Session session() {
        return cluster().connect(keyspace);
    }
}
