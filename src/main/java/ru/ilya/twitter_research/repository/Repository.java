package ru.ilya.twitter_research.repository;


public interface Repository<T> {

    void insert(T entity);

}
