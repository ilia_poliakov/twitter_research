package ru.ilya.twitter_research.repository;

import com.datastax.driver.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.ilya.twitter_research.domain.Tweet;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.StreamSupport.stream;

@Repository
public class CassandraTweetRepository implements TweetRepository {

    private static String SELECT_BY_TOPIC = "SELECT * FROM tweet WHERE topic = :topic";

    private static String SELECT_BY_DATE_RANGE = "SELECT * FROM tweet WHERE topic = :topic AND created_at <= :end_date AND created_at >= :start_date";

    private static String INSERT_TWEET_QUERY = "INSERT INTO tweet " +
            "(id, topic, original_text, normalized_text, sentiment_rate, hash_tags, created_at) " +
                "VALUES " +
            "(:id, :topic, :original_text, :normalized_text, :sentiment_rate, :hash_tags, :created_at)";

    private Session session;
    private PreparedStatement insertTweetPreparedStatement;
    private PreparedStatement selectByTopicPreparedStatement;
    private PreparedStatement selectByDateRangePreparedStatement;

    @Autowired
    public CassandraTweetRepository(Session session) {
        this.session = session;
        this.insertTweetPreparedStatement = session.prepare(INSERT_TWEET_QUERY);
        this.selectByTopicPreparedStatement = session.prepare(SELECT_BY_TOPIC);
        this.selectByDateRangePreparedStatement = session.prepare(SELECT_BY_DATE_RANGE);
    }

    @Override
    public List<Tweet> findByTopic(String topic) {
        BoundStatement bs = selectByTopicPreparedStatement.bind()
                .setString("topic", topic);

        return fetchAllTweets(bs);
    }

    @Override
    public List<Tweet> findByDateRangeFromTopic(String topic, Date from, Date to) {
        BoundStatement bs = selectByDateRangePreparedStatement.bind()
                .setString("topic", topic)
                .setDate("start_date", LocalDate.fromMillisSinceEpoch(from.getTime()))
                .setDate("end_date", LocalDate.fromMillisSinceEpoch(to.getTime()));
        return fetchAllTweets(bs);
    }

    @Override
    public void insert(Tweet tweet) {
        BoundStatement boundStatement = insertTweetPreparedStatement.setConsistencyLevel(ConsistencyLevel.ANY).bind()
                .setLong("id", tweet.getId())
                .setString("topic", tweet.getTopic())
                .setString("original_text", tweet.getOriginalText())
                .setString("normalized_text", tweet.getNormalizedText())
                .setDouble("sentiment_rate", tweet.getSentimentRate())
                .setList("hash_tags", Arrays.asList(tweet.getHashTags()))
                .setDate("created_at", LocalDate.fromMillisSinceEpoch(tweet.getCreatedAt().getTime()));
        session.execute(boundStatement);
    }

    private List<Tweet> fetchAllTweets(BoundStatement bs) {
        return stream(session.execute(bs).spliterator(), false)
                .map(this::extractTweet)
                .collect(Collectors.toList());
    }

    private Tweet extractTweet(Row row) {
        return new Tweet(row.getLong("id"),
                row.getString("topic"),
                row.getString("original_text"),
                row.getString("normalized_text"),
                row.getDouble("sentiment_rate"),
                row.getSet("hash_tags", String.class).stream().toArray(String[]::new),
                new Date(row.getDate("created_at").getMillisSinceEpoch()));
    }
}
