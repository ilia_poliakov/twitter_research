package ru.ilya.twitter_research.repository;

import ru.ilya.twitter_research.domain.Tweet;

import java.util.Date;
import java.util.List;

public interface TweetRepository extends Repository<Tweet> {

    List<Tweet> findByTopic(String topic);

    List<Tweet> findByDateRangeFromTopic(String topic, Date from, Date to);

}
