package ru.ilya.twitter_research;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.ilya.twitter_research.stages.PreprocessingStage;
import ru.ilya.twitter_research.stages.SavingToDbStage;
import ru.ilya.twitter_research.stages.StatusSentimentEstimatingStage;

import java.util.concurrent.ExecutorService;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config/appContext.xml");

        PreprocessingStage preprocessingStage = context.getBean("preprocessingStage", PreprocessingStage.class);
        StatusSentimentEstimatingStage statusSentimentEstimatingStage = context.getBean("statusSentimentEstimatingStage", StatusSentimentEstimatingStage.class);
        SavingToDbStage savingToDbStage = context.getBean("savingToDbStage", SavingToDbStage.class);

        ExecutorService preprocessingStageThreadPool = context.getBean("preprocessingStageThreadPool", ThreadPoolTaskExecutor.class).getThreadPoolExecutor();
        ExecutorService statusSentimentEstimatingStageThreadPool = context.getBean("statusSentimentEstimatingStageThreadPool", ThreadPoolTaskExecutor.class).getThreadPoolExecutor();
        ExecutorService savingToDbStageThreadPool = context.getBean("savingToDbStageThreadPool", ThreadPoolTaskExecutor.class).getThreadPoolExecutor();

        preprocessingStageThreadPool.execute(preprocessingStage);
        statusSentimentEstimatingStageThreadPool.execute(statusSentimentEstimatingStage);
        savingToDbStageThreadPool.execute(savingToDbStage);

        logger.info("stages started");

    }
}
