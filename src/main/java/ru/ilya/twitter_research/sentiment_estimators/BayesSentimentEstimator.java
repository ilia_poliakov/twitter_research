package ru.ilya.twitter_research.sentiment_estimators;

import com.google.re2j.Pattern;
import ru.ilya.twitter_research.domain.Word;
import ru.ilya.twitter_research.utils.loaders.MapLoader;

import java.util.Map;
import java.util.stream.Stream;

import static java.lang.Math.log;


public class BayesSentimentEstimator implements SentimentEstimator {

    private static final Pattern SPLIT_BY_WHITESPACE = Pattern.compile(" ");
    private static final double DOCUMENT_LOG_ESTIMATION = log(1.0/2.0);
    private static final double COUNT_POSITIVE_WORDS = 63611;
    private static final double COUNT_NEGATIVE_WORDS = 53871;

    private final Map<String, Word> wordFrequency;

    public BayesSentimentEstimator(MapLoader<Word> wordFrequencyLoader) {
        wordFrequency = wordFrequencyLoader.loadMap();
    }

    @Override
    public double estimate(String text) {
        String[] words = SPLIT_BY_WHITESPACE.split(text);

        double estimateForPositive = Stream.of(words)
                .mapToDouble(w -> log((wordFrequency.getOrDefault(w, new Word(w))
                        .getOccurrenceInPositive() + 1.0) /
                        (wordFrequency.size() + COUNT_POSITIVE_WORDS)))
                .sum() + DOCUMENT_LOG_ESTIMATION;

        double estimateForNegative = Stream.of(words)
                .mapToDouble(w -> log((wordFrequency.getOrDefault(w, new Word(w))
                        .getOccurrenceInNegative() + 1.0) /
                        (wordFrequency.size() + COUNT_NEGATIVE_WORDS)))
                .sum() + DOCUMENT_LOG_ESTIMATION;

        return 1.0 / (1 + Math.exp(estimateForNegative - (estimateForPositive)));
    }

}
