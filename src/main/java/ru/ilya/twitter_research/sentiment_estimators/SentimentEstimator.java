package ru.ilya.twitter_research.sentiment_estimators;

public interface SentimentEstimator {

    public double estimate(String text);
}
