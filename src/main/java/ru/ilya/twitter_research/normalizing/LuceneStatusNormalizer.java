package ru.ilya.twitter_research.normalizing;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Closeable;
import java.io.IOException;
import java.io.StringReader;

@Component("luceneStatusNormalizer")
public class LuceneStatusNormalizer implements Normalizer<String, String> {

    private Logger logger = LoggerFactory.getLogger(LuceneStatusNormalizer.class);

    @Autowired
    private Analyzer analyzer;

    @Override
    public String normalize(String entityToNormalizing) {
        if (entityToNormalizing == null) {
            return "";
        }
        TokenStream tokenStream = null;
        try {
            tokenStream = analyzer.tokenStream("content", new StringReader(entityToNormalizing));
            CharTermAttribute term = tokenStream.addAttribute(CharTermAttribute.class);
            tokenStream.reset();
            StringBuilder result = new StringBuilder();
            while (tokenStream.incrementToken()) {
                result.append(term.toString())
                        .append(" ");
            }
            return result
                    .toString()
                    .trim();

        } catch (IOException e) {
            logger.error("error while normalizing", e);
        } finally {
            closeResource(tokenStream);
        }
        return "";
    }

    private void closeResource(Closeable closeable) {
        try {
            if (closeable != null)
                closeable.close();
        } catch (IOException e) {
            logger.error("error while closing resource" + closeable.toString(), e);
        }
    }
}
