package ru.ilya.twitter_research.normalizing;


public interface Normalizer<T, R> {

    public R normalize(T entityToNormalizing);
}
