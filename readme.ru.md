# Twitter research

## Описание
Система мониторинга общественного мнения в twitter по заданным тематикам.  
Приложение в режиме реального времени прослушивает twitter stream по заданным ключевым словам или тегам,
автоматически дает оценку тональности твита и сохраняет в базу данных.

## Используемые технологии
java 8, spring framework, multithreading, apache cassandra.  
Delta TF-IDF алгоритм для формирования вектора признаков текста.  
Своя реализация Баесовского классификатора для оценки тональности твитов.

## Запуск
1. Запустить apache cassandra
2. Выполнить скрипт ```src/main/resources/database_config/cassandra_init.sql``` с помощью cqlsh: ```cqlsh -f cassandra_init.sql```
3. Собрать проект: ```mvn clean install```
4. Запустить ```java -jar twitter_research.jar```